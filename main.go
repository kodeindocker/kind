package main

import (
	// "fmt"
	"embed"
	"os"
	"strconv"

	flags "github.com/jessevdk/go-flags"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"

	// "gitlab.com/kodeindocker/kind/api"
	"gitlab.com/kodeindocker/kind/cmd"
)

//go:embed web/dist
var frontendFS embed.FS

func init() {
	zerolog.TimeFieldFormat = zerolog.TimeFormatUnix
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))
	if err != nil {
		logLevel = 0
	}
	zerologLevel := zerolog.Level(logLevel)
	zerolog.SetGlobalLevel(zerologLevel)
	log.Logger = log.With().Caller().Logger()
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})
	cmd.FrontendFS = frontendFS
}

func main() {
	var parser = flags.NewParser(nil, flags.Default)

	var upCmd cmd.Up
	var psCmd cmd.Ps
	var downCmd cmd.Down
	var restartCmd cmd.Restart
	var logCmd cmd.Logs
	var stopCmd cmd.Stop
	var startCmd cmd.Start
	var configCmd cmd.Config
	var serveCmd cmd.Serve
	// var shareCmd cmd.Share
	// var joinCmd cmd.Join

	parser.AddCommand("ps",
		"Listar Entornos de Desarrollo",
		"Este comando es utilizado para listar entornos de desarrollos activos e inactivos.",
		&psCmd)

	parser.AddCommand("up",
		"Inicar nuevo Entorno",
		"Este comando es utilizado para iniciar un nuevo entorno de desarrollo.",
		&upCmd)

	parser.AddCommand("down",
		"Elimina un Entorno",
		"Este comando detiene/elimina un entorno de desarrollo.",
		&downCmd)

	parser.AddCommand("restart",
		"Reinicia un Entorno / Servicio",
		"Reinicia un Entorno / Servicio.",
		&restartCmd)

	parser.AddCommand("stop",
		"Detiene un Entorno",
		"Este comando detiene un entorno de desarrollo.",
		&stopCmd)

	parser.AddCommand("start",
		"Inicia un Entorno existente",
		"Inicia un Entorno existente.",
		&startCmd)

	parser.AddCommand("logs",
		"Visualización de logs",
		"Este comando muestra los logs generados por el entorno especificado.",
		&logCmd)

	parser.AddCommand("config",
		"Visualización de logs",
		"Genera archivo config.yml",
		&configCmd)

	parser.AddCommand("serve",
		"Inicia Frontend WEB para KinD",
		"Inicia Frontend WEB para KinD.",
		&serveCmd)

	// parser.AddCommand("share",
	// 	"Comparte entorno via ssh",
	// 	"Inicia un tunel ssh con un concentrador para compartir los puertos del entorno.",
	// 	&shareCmd)

	// parser.AddCommand("join",
	// 	"Se conecta a un entorno compartido",
	// 	"Inicia un tunel ssh con un concentrador para conectarse con un entorno compartido.",
	// 	&joinCmd)

	if _, err := parser.Parse(); err != nil {
		os.Exit(1)
	}

}
