import { createRouter, createWebHashHistory } from "vue-router";

const routes = [
  {
    path: "/",
    name: "Workspaces",
    component: () => import(/* webpackChunkName: "environments" */ "../views/WorkspacesView.vue"),
    meta: {
      nav: {
        icon: "el-icon-s-home"
      },
			view: {
				title: 'Entornos',
				titleNew: 'Entornos',
			},
		},
  },
  {
    path: "/templates",
    name: "Plantillas",
    component: () => import(/* webpackChunkName: "templates" */ "../views/TemplatesView.vue"),
    meta: {
      nav: {
        title: 'Plantillas',
        icon: "el-icon-document-copy"
      }
    },
		children: [
			{
				path: ':id',
				name: 'Plantilla',
				component: () => import(/* webpackChunkName: "templates" */ '../views/TemplateView.vue'),
				meta: {
					view: {
						title: 'Editar Plantilla',
						titleNew: 'Crear Plantilla',
					},
				}
			},
		]
  },
  {
    path: "/volumes",
    name: "Volúmenes",
    component: () => import(/* webpackChunkName: "misc" */ "../views/VolumesView.vue"),
    meta: {
      nav: {
				menu: "Avanzado",
        title: 'Volúmenes',
        icon: "el-icon-setting"
      }
    },
		children: [
			// {
			// 	path: ':id',
			// 	name: 'Plantilla',
			// 	component: () => import(/* webpackChunkName: "templates" */ '../views/TemplateView.vue'),
			// 	meta: {
			// 		view: {
			// 			title: 'Editar Plantilla',
			// 			titleNew: 'Crear Plantilla',
			// 		},
			// 	}
			// },
		]
  },
	{
    path: "/about",
    name: "about",
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/AboutView.vue"),
    meta: {
      nav: {
        icon: "el-icon-info"
      }
    }
  },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});

export default router;
