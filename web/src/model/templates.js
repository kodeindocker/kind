import {Model} from 'mind-core-vue3'

class M extends Model {
	constructor() {
		super({
			uri:'api/templates',
			key: 'name',
			cacheTimeout: 0,
			template: {
        image: "",
        description: "",
        variant: "alpine",
        lang: "",
        sudo: false,
        dind: {
          enabled: false,
          mode: "bind"
        },
        tasks: {
          init: [],
          command: []
        },
        gitlab: {
          createMR: true
        },
        git: {
          url: "",
          url2: "",
          username: "",
          password: "",
          fullname: "",
          email: "",
          branch: ""
        },
				ingress:{
					enabled: false,
				},
				ports: {
					ide: "",
					ingress: "auto",
						extra: []
        },
        env: { },
        volumes: {}
      }
		})
	}
    post(id, payload) {
		return fetch(`${this.uri}/${id}`, { method: 'POST', credentials: "same-origin", body: JSON.stringify(payload) })
    }
}


export default new M()