import {Model} from 'mind-core-vue3'

class M extends Model {
	constructor() {
		super({
			uri:'api/volumes',
			key: 'name',
			cacheTimeout: 0,
			template: {
				name: "",
				description: ""
      }
		})
	}
}


export default new M()