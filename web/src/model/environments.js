import {Model, fetch} from 'mind-core-vue3'

class M extends Model {
	constructor() {
		super({
			uri:'api/environments',
			key: 'name',
			cacheTimeout: 0,
			template: {
      }
		})
	}
	stop(id) {
		return fetch(`${this.uri}/${id}/stop`, { method: 'PUT', credentials: "same-origin", })
	}
	start(id) {
		return fetch(`${this.uri}/${id}/start`, { method: 'PUT', credentials: "same-origin", })
	}
	restart(id) {
		return fetch(`${this.uri}/${id}/restart`, { method: 'PUT', credentials: "same-origin", })
	}
	delete(id) {
		return fetch(`${this.uri}/${id}`, { method: 'DELETE', credentials: "same-origin", })
	}
	wait(id) {
		return fetch(`${this.uri}/${id}/wait`, { method: 'GET', credentials: "same-origin", })
	}
	post(payload) {
		return fetch(`${this.uri}`, { method: 'POST', credentials: "same-origin", body: JSON.stringify(payload) })
	}
}



export default new M()