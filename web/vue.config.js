const { defineConfig } = require("@vue/cli-service");
module.exports = defineConfig({
  transpileDependencies: true,
	publicPath: './',
	devServer: {
		// disableHostCheck: true,
		https: false,
		port: 8080,
		proxy: {
			'/api': {
				target: `${process.env.API_PROTOCOL || 'http'}://${(process.env.API_HOST || '127.0.0.1')}:${process.env.API_PORT || '3100'}`,
				// secure: false,
				// followRedirects: true,
				// autoRewrite: true,
				// xfwd: true,
				// changeOrigin: true,
				// toProxy: true,
			},
		}
	},
});
