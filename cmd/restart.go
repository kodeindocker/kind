//Package cmd contiene la definición de los comandos disponibles desde linea de comandos
package cmd

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/kodeindocker/kind/docker"
)

//Restart Comando para la visualización de lgos
type Restart struct {
	Args restartArgs `positional-args:"yes"`
}

type restartArgs struct {
	Entorno  string `description:"Nombre del Entorno." required:"yes"`
	Servicio string `description:"Nombre del Servicio a Reiniciar." `
}

//Execute Muestra el log del entorno especificado
func (x *Restart) Execute(args []string) error {
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al crear contenedor")
		return err
	}
	log.Info().Interface("entorno", x.Args.Entorno).Msg("Reiniciando entorno")

	err = cli.ContainerRestartByName(x.Args.Entorno)
	if err != nil {
		return err
	}

	return nil
}
