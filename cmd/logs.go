//Package cmd contiene la definición de los comandos disponibles desde linea de comandos
package cmd

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/kodeindocker/kind/docker"
	"io"
	"os"
)

//Logs Comando para la visualización de lgos
type Logs struct {
	Follow bool     `short:"f" long:"follow" description:"Muestra el log a medida que va creciendo" `
	Tail   string   `short:"t" long:"tail" description:"mostrar solo las ultimas n lineas" `
	Args   logsArgs `positional-args:"yes"`
}

type logsArgs struct {
	Entorno string `description:"Nombre del Entorno." required:"yes"`
}

//Execute Muestra el log del entorno especificado
func (x *Logs) Execute(args []string) error {
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("")
		return err
	}
	log.Debug().Interface("Entorno", x.Args.Entorno).Interface("Follow", x.Follow).Interface("Tail", x.Tail).Msg("Logs")

	reader, err := cli.Logs(x.Args.Entorno, x.Follow, x.Tail)
	if err != nil {
		log.Error().Err(err).Msg("")
	}
	defer reader.Close()
	io.Copy(os.Stdout, reader)

	return nil

}
