//Package cmd contiene la definición de los comandos disponibles desde linea de comandos
package cmd

import (
	"fmt"

	"github.com/rs/zerolog/log"
	"gitlab.com/kodeindocker/kind/docker"
)

//Ps Comando para el listado de entornos
type Ps struct {
	Args psArgs `positional-args:"yes"`
}

type psArgs struct {
	Entorno string `description:"Entorno del Proyecto. Si se especifica, se muestra el estados de los contenedores de dicho proyecto" optional:"yes"`
}

//Execute Lista los entornos
func (x *Ps) Execute(args []string) error {
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al crear contenedor")
		return err
	}
	containers, err := cli.Ps(x.Args.Entorno)

	fmt.Printf("%-15.15s %-12.12s %-15s %s\n", "Entorno", "Contenedor", "Estado", "Puertos")
	fmt.Printf("----------------------------------------------------------------------------------------------\n")
	for _, c := range containers {
		name := c.Names[0][7:]
		puertos := ""
		for _, p := range c.Ports {
			puertos += fmt.Sprintf("%s:%d->%d ", p.IP, p.PublicPort, p.PrivatePort)
		}
		fmt.Printf("%-15.15s %-12.12s %-15s %s\n", name, c.ID, c.State, puertos)
	}
	fmt.Println("")

	return nil
}
