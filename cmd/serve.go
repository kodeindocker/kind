//Package cmd ....
package cmd

import (
	"embed"
	"errors"
	"fmt"
	"io/fs"
	"net/http"
	"os"
	"strings"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/rs/zerolog/log"

	"gitlab.com/kodeindocker/kind/api"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
)

var FrontendFS embed.FS

//Serve Comando Unirse a sesion compartida
type Serve struct {
	Host string `short:"H" long:"host" description:"Host de escucha" default:"0.0.0.0"`
	Port int    `short:"p" long:"puerto" description:"Puerto de Escucha" default:"3100"`
}

//Execute Lista los entornos
func (x *Serve) Execute(args []string) error {

	home := os.Getenv("HOME")
	templateFolder := os.Getenv("HOME") + "/.KodeInDocker/templates"

	if _, err := os.Stat(templateFolder); errors.Is(err, os.ErrNotExist) {
		if err := os.MkdirAll(templateFolder, fs.ModePerm); err != nil {
			log.Error().Err(err).Msg("Error al crear carpeta de templates")
			return err
		}
	}

	log.Info().Msg("Iniciando KodeInDocker en Modo servicio")
	filename := home + "/.KodeInDocker/config.yml"
	if _, err := os.Stat(filename); errors.Is(err, os.ErrNotExist) {
		filename = "/etc/KodeInDocker/config.yml"
		if _, err := os.Stat(filename); errors.Is(err, os.ErrNotExist) {
			log.Error().Msg("No se pudo cargar configuración. Genere la configuración utilizando kind [-g] config")
			return err
		}
	}

	r := chi.NewRouter()
	r.Use(render.SetContentType(render.ContentTypeJSON))
	r.Use(middlewares.ParseParamsCtx)

	fsRoot, err := fs.Sub(FrontendFS, "web/dist")
	if err != nil {
		log.Error().Err(err).Msg("Error al carger frontend")
		return err
	}
	FileServer(r, "/", http.FS(fsRoot))

	r.Mount("/api/templates", api.Templates(templateFolder))
	r.Mount("/api/environments", api.Workspaces(templateFolder))
	r.Mount("/api/volumes", api.Volumes())

	port := fmt.Sprintf(":%d", x.Port)
	log.Debug().Str("port", port).Msg("Iniciando Servicio")
	http.ListenAndServe(port, r)

	return nil
}

// FileServer conveniently sets up a http.FileServer handler to serve
// static files from a http.FileSystem.
func FileServer(r chi.Router, path string, root http.FileSystem) {
	if strings.ContainsAny(path, "{}*") {
		panic("FileServer does not permit any URL parameters.")
	}

	if path != "/" && path[len(path)-1] != '/' {
		r.Get(path, http.RedirectHandler(path+"/", http.StatusMovedPermanently).ServeHTTP)
		path += "/"
	}
	path += "*"

	r.Get(path, func(w http.ResponseWriter, r *http.Request) {
		rctx := chi.RouteContext(r.Context())
		pathPrefix := strings.TrimSuffix(rctx.RoutePattern(), "/*")
		fs := http.StripPrefix(pathPrefix, http.FileServer(root))
		fs.ServeHTTP(w, r)
	})
}
