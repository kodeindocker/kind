//Package cmd contiene la definición de los comandos disponibles desde linea de comandos
package cmd

import (
	"context"
	"github.com/rs/zerolog/log"
	"gitlab.com/kodeindocker/kind/docker"
)

//Start Comando para la visualización de lgos
type Start struct {
	Args startArgs `positional-args:"yes"`
}

type startArgs struct {
	Entorno  string `description:"Nombre del Entorno." required:"yes"`
	Servicio string `description:"Nombre del Servicio a Reiniciar." `
}

//Execute Inicia un contenedor
func (x *Start) Execute(args []string) error {
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al crear contenedor")
		return err
	}
	log.Info().Interface("entorno", x.Args.Entorno).Msg("Reiniciando entorno")

	ctx := context.Background()

	err = cli.ContainerStartByName(ctx, x.Args.Entorno)
	if err != nil {
		return err
	}

	return nil
}
