//Package cmd contiene la definición de los comandos disponibles desde linea de comandos
package cmd

import (
	_ "embed"
	"io/fs"
	"os"

	"github.com/rs/zerolog/log"
)

//Config Comando para el listado de entornos
type Config struct {
	Force bool     `long:"force" description:"Forzar Generación" `
	Global bool     `short:"g" global:"global" description:"Generar Configuración en forma global" `
}

//go:embed "config.yml"
var configBytes []byte

//Execute Lista los entornos
func (x *Config) Execute(args []string) error {

	dst := ""
	if x.Global {
		dst = "/etc/KodeInDocker"
	} else {
		dst = os.Getenv("HOME") + "/.KodeInDocker"
	}

	filename := dst + "/config.yml"

	if err := os.MkdirAll(dst, fs.ModePerm); err != nil {
		log.Error().Err(err).Msg("Error al crear carpeta de configuracion")
		return err
	}

	// si el archivo existe y no esta forzado genera un error
	if _, err := os.Stat(filename); err == nil && !x.Force {
		log.Error().Msg("Error al generar configuración, archivo ya existe. utilice --force para forzar")
		return err
	}

	
	if err := os.WriteFile(filename, configBytes, 0666); err != nil {
			log.Error().Err(err).Msg("Error al leer configuración")
			return err
	}

	log.Debug().Str("destino", filename).Msg("Configuración Generada correctamente")
	return nil
}
