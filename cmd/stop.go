//Package cmd contiene la definición de los comandos disponibles desde linea de comandos
package cmd

import (
	"github.com/rs/zerolog/log"
	"gitlab.com/kodeindocker/kind/docker"
)

//Stop Comando para la visualización de lgos
type Stop struct {
	Args stopArgs `positional-args:"yes"`
}

type stopArgs struct {
	Entorno string `description:"Nombre del Entorno." required:"yes"`
}

//Execute Muestra el log del entorno especificado
func (x *Stop) Execute(args []string) error {
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al crear contenedor")
		return err
	}
	log.Info().Interface("entorno", x.Args.Entorno).Msg("Reiniciando entorno")

	err = cli.ContainerStopByName(x.Args.Entorno)
	if err != nil {
		return err
	}

	return nil
}
