//Package cmd contiene la definición de los comandos disponibles desde linea de comandos
package cmd

import (
	"sort"
	"strconv"

	"github.com/rs/zerolog/log"

	"gitlab.com/kodeindocker/kind/docker"
	"gitlab.com/kodeindocker/kind/loader"
)

//Up Comando para la Inicialización del Entorno de Desarrollo
type Up struct {
	Envs []string `short:"e" long:"environment" description:"Variables de entorno en formato variable=valor" `
	// GitURL   gitURL     `short:"g" long:"git-url" description:"URL del repositorio Git (https)"  `
	Pull     bool       `long:"pull" description:"fuerza actualización de la imagen" required:"no"`
	Template string     `short:"t" long:"template" description:"path a plantilla de ejecución de entorno" required:"yes"`
	Args     argumentos `positional-args:"yes"`
}

type argumentos struct {
	Nombre string `description:"Nombre del Proyecto" required:"yes"`
}

//Execute Inicia el Entorno de Desarrollo
func (x *Up) Execute(args []string) error {
	log.Info().Str("Entorno", x.Args.Nombre).Msg("Iniciando Entorno de Desarrollo")

	template, err := loader.LoadTemplate(x.Template)
	if err != nil {
		log.Error().Err(err).Msg("Error al cargar template")
		return err
	}

	docker.CreateWorkspace(x.Args.Nombre, template, x.Pull, true)

	return nil
}

func findFreePort(list []int, candidate string) string {
	cand, err := strconv.Atoi(candidate)
	if err != nil {
		cand = 3000
	}
	for {
		cand++
		pos := sort.SearchInts(list, cand)
		if pos >= len(list) {
			return strconv.Itoa(cand)
		}
	}

}
