//Package cmd contiene la definición de los comandos disponibles desde linea de comandos
package cmd

import (
	"fmt"
	"github.com/rs/zerolog/log"
	"gitlab.com/kodeindocker/kind/docker"
)

//Down Comando para la visualización de lgos
type Down struct {
	Force bool     `long:"force" description:"Forzar detención del Entorno" `
	Args  downArgs `positional-args:"yes"`
}

type downArgs struct {
	Entorno string `description:"Nombre del Entorno." required:"yes"`
}

//Execute Muestra el log del entorno especificado
func (x *Down) Execute(args []string) error {
	fmt.Printf("Deteniendo entorno: '%v'\n", x.Args.Entorno)

	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al detener contenedor")
		return err
	}

	err = cli.ContainerDown(x.Args.Entorno, x.Force)
	if err != nil {
		fmt.Println("Error Al intentar Eliminar el Entorno de desarrollo: ")
		return err
	}

	fmt.Printf("Se Eliminó el Entorno: '%v'\n", x.Args.Entorno)

	return nil
}
