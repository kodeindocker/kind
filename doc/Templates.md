### Templates
KiND uses templates to generate development environments.
Each template is a yaml file like the following
```yml
# image: registry.gitlab.com/kodeindocker/theia-test # overrides default theia image
variant: alpine #alpine is the default os variant
lang: go
dind:
  enabled: true # enables docker in docker
  mode: link  #use link mode (links parent's /var/run/docker)
git: 
  url: https://gitlab.com/kodeindocker/kind.git # url of the git repo to clone
  username: my_username # an username for authentication, defaults to ${GIT_USERNAME}
  password: password # password for athentication, defaults to ${GIT_PASSWORD}
  fullname: Cosme Fulanito # name used for setting 'git.username', defaults to ${GIT_FULLNAME}
  email: me@example.com # email used for setting 'git.email', defaults to ${GIT_EMAIL}
  branch: main  # branch to pull, if blank pull the default branch
gitlab:
  create-mr: true
ports:
  - {ip: 0.0.0.0, external: 6001, internal: 3000, protocol: https}
  - {ip: 0.0.0.0, external: 6002, internal: 8080, protocol: http}  
  - {ip: 0.0.0.0, external: 6003, internal: 8081}  
env:
  LOG_LEVEL: 0
  API_HOST: 10.1.1.1

```