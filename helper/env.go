//Package helper ...
package helper

import (
	// "bufio"
	// log "github.com/rs/zerolog/log"
	// "os"
	"strings"
)

/*
//LoadEnvFile Carga archivo de entorno, y devuelve un slice con cada linea del archivo
func LoadEnvFile(filename string) []string {
	var ret []string
	file, err := os.Open(filename)
	if err != nil {
		log.Error().Err(err).Send()
		return nil
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if line == "" || line[0] == '#' {
			continue
		}
		ret = append(ret, line)
		// fmt.Println(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Error().Err(err).Send()
		return nil
	}
	return ret
}
*/
/*
//LoadEnvFileAsMap Carga archivo de entorno, y devuelve un map con las variables de entorno
func LoadEnvFileAsMap(filename string) map[string]string {
	lines := LoadEnvFile(filename)
	ret := EnvToMap(lines)
	return ret
}
*/
/*
//EnvToMap recibe un slice con variables de entornos en formato VAR=VAL
//devuelve un map con las variables y sus valores
func EnvToMap(vars []string) map[string]string {
	var ret = make(map[string]string)
	for _, l := range vars {
		k, v := ParseEnvLine(l)
		if k == "" {
			continue
		}
		ret[k] = v
	}
	return ret
}
*/
//EnvMapToSlice recibe una lista de variables en formato de map
// devuelve un slice con el formato [VAR=VAL, VAR=VAL]
func EnvMapToSlice(vars map[string]string) []string {
	ret := make([]string, 0)
	for k, v := range vars {
		ret = append(ret, k+"="+v)
	}
	return ret
}

// ReplaceVars busca en text todas las variables especificadas en vars y las reemplaza por su valor
// Por ej
//    text= "hola ${mundo}" y vars["mundo"] = "manuel", retorna "hola manuel"
func ReplaceVars(text string, vars map[string]string) string {
	for k, v := range vars {
		text = strings.ReplaceAll(text, "${"+k+"}", v)
	}
	return text

}

// MergeMaps une dos maps y devuelve el resultado
// Nota, si hay keys duplicados, los valores del map2 son utilizados
func MergeMaps(map1 map[string]string, map2 map[string]string) map[string]string {
	for k, v := range map2 {
		map1[k] = v
	}
	return map1
}
