package docker

import (
	// "context"
	"github.com/docker/docker/client"
	"github.com/rs/zerolog/log"
)

//instance contiene la referencia a una conexión global a docker
var instance *Connection

func init() {
	var err error
	instance, err = NewConnection()
	if err != nil {
		log.Fatal().Err(err).Msg("Error al conectar con docker!")
	}

}

//GetInstance devuelve una conexión a docker, reutilizando una si existe
func GetInstance() (*Connection, error) {
	if instance == nil {
		var err error
		instance, err = NewConnection()
		if err != nil {
			return nil, err
		}
	}
	return instance, nil
}

//NewConnection returns a new connection to docker client
func NewConnection() (*Connection, error) {
	// ctx := context.Background()
	cli, err := client.NewClientWithOpts(client.FromEnv, client.WithAPIVersionNegotiation())
	if err != nil {
		return nil, err
	}

	return &Connection{
		Client: cli,
	}, nil
}

//Connection represents an connection to a docker service
type Connection struct {
	*client.Client
}
