package docker

import (
	"context"
	// "errors"
	"github.com/docker/docker/api/types"
)

//ContainerExec Inicia un contenedor
func (c *Connection) ContainerExec(ctx context.Context, id string, command []string) error {

	opts := types.ExecConfig{
		AttachStderr: true,
		AttachStdout: true,
		Cmd:          command,
		User:         "root",
	}
	r, err := c.ContainerExecCreate(ctx, id, opts)
	if err != nil {
		return nil
	}
	return c.ContainerExecStart(ctx, r.ID, types.ExecStartCheck{Detach: true})

}
