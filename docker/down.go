package docker

import (
	"context"
	"errors"
	"github.com/docker/docker/api/types"
)

//ContainerDown Detiene y Elimina un contenedor
func (c *Connection) ContainerDown(name string, force bool) error {
	containers, err := c.Ps(name)
	if err != nil {
		return err
	}
	if len(containers) == 0 {
		return errors.New("Entorno '" + name + "' no encontrado")
	}
	ctx := context.Background()
	id := containers[0].ID

	err = c.ContainerRemove(ctx, id, types.ContainerRemoveOptions{
		// RemoveLinks: true,
		RemoveVolumes: true,
		Force:         force,
	})
	if err != nil {
		return err
	}

	return nil
}
