package docker

import (
	"context"
	"errors"
	// "github.com/docker/docker/api/types"
)

//ContainerStopByName Detiene un Contenedor
func (c *Connection) ContainerStopByName(name string) error {
	containers, err := c.Ps(name)
	if err != nil {
		return err
	}
	if len(containers) == 0 {
		return errors.New("Entorno '" + name + "' no encontrado")
	}
	ctx := context.Background()
	id := containers[0].ID

	err = c.ContainerStop(ctx, id, nil)
	if err != nil {
		return err
	}

	return nil
}
