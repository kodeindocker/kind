package docker

import (
	"context"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
	volumetypes "github.com/docker/docker/api/types/volume"
)

//FindVolume busca un volumen con el nombre especificado
func (c *Connection) FindVolume(name string) (volume *types.Volume, err error) {
	volumes, err := c.Client.VolumeList(context.Background(), filters.NewArgs())

	if err != nil {
		return nil, err
	}

	for _, v := range volumes.Volumes {
		if v.Name == name {
			return v, nil
		}
	}
	return nil, nil
}

//VolumeCreateLocal crea un volumen con driver local, si no existe
func (c *Connection) VolumeCreateLocal(name string, description string) (created bool, volume *types.Volume, err error) {
	volume, err = c.FindVolume(name)

	if err != nil {
		return false, nil, err
	}

	if volume != nil {
		return false, volume, nil
	}

	vol, err := c.Client.VolumeCreate(context.Background(), volumetypes.VolumeCreateBody{
		Driver: "local",
		//		DriverOpts: map[string]string{},
		Labels: map[string]string{
			"description": description,
		},
		Name: name,
	})

	return true, &vol, err
}

//RemoveVolume elimina el volumen especificado por el parametro name
func (c *Connection) VolumeRemoveIfExists(ctx context.Context, name string) (removed bool, err error) {
	vol, err := c.FindVolume(name)
	if err != nil {
		return false, err
	}

	if vol == nil {
		return false, nil
	}

	err = c.Client.VolumeRemove(ctx, name, true)

	if err != nil {
		return false, err
	}

	return true, nil
}
