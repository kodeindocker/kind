package docker

import (
	"context"
	"errors"
	"github.com/docker/docker/api/types"
	"io"
)

//Logs Reinicia un Contenedor
func (c *Connection) Logs(name string, follow bool, tail string) (io.ReadCloser, error) {
	containers, err := c.Ps(name)
	if err != nil {
		return nil, err
	}
	if len(containers) == 0 {
		return nil, errors.New("Entorno '" + name + "' no encontrado")
	}
	ctx := context.Background()
	id := containers[0].ID

	opts := types.ContainerLogsOptions{
		ShowStdout: true,
		// ShowStderr: true,
		Follow: follow,
		Tail:   tail,
	}
	return c.Client.ContainerLogs(ctx, id, opts)
}
