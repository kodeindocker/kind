package docker

import (
	"context"
	"errors"
	"github.com/docker/docker/api/types"
)

//ContainerStartByName Inicia un contenedor
func (c *Connection) ContainerStartByName(ctx context.Context, name string) error {
	containers, err := c.Ps(name)
	if err != nil {
		return err
	}
	if len(containers) == 0 {
		return errors.New("Entorno '" + name + "' no encontrado")
	}
	container := containers[0]
	id := container.ID

	if err := c.ContainerStart(ctx, id, types.ContainerStartOptions{}); err != nil {
		return err
	}

	dind := container.Labels["KodeInDocker.dind"]
	if dind == "true" {
		if err := StartDockerd(id); err != nil {
			return err
		}
	}

	return nil
}
