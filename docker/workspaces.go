package docker

import (
	"archive/tar"
	"bytes"
	"context"
	"errors"
	// "fmt"
	"io"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/container"
	"github.com/docker/docker/api/types/filters"
	"github.com/docker/docker/api/types/mount"
	"github.com/docker/docker/api/types/network"
	natting "github.com/docker/go-connections/nat"
	specs "github.com/opencontainers/image-spec/specs-go/v1"
	"github.com/rs/zerolog/log"

	"gitlab.com/kodeindocker/kind/helper"
	"gitlab.com/kodeindocker/kind/loader"
)

// CreateWorkspace crea un nuevo entorno
func CreateWorkspace(name string, template *loader.TemplateConfig, pull bool, wait bool) error {
	log.Info().Str("Entorno", name).Msg("Iniciando Entorno de Desarrollo")
	var extraHosts []string
	ctx := context.Background()

	if name == "" {
		return errors.New("nombre de entorno inválido")
	}

	home := os.Getenv("HOME")
	filename := home + "/.KodeInDocker/config.yml"
	if _, err := os.Stat(filename); errors.Is(err, os.ErrNotExist) {
		filename = "/etc/KodeInDocker/config.yml"
		if _, err := os.Stat(filename); errors.Is(err, os.ErrNotExist) {
			log.Error().Msg("No se pudo cargar configuración. Genere la configuración utilizando kind [-g] config")
			return err
		}
	}
	log.Debug().Interface("Filename", filename).Msg("Leyendo configuración")
	config, err := loader.LoadConfig(filename)
	if err != nil {
		log.Error().Err(err).Msg("Error al leer configuración")
		return err
	}

	if err := template.ParseVars(config.Env); err != nil {
		log.Error().Err(err).Msg("Error al parsear variables")
		return err
	}

	var envs []string
	//carga de variables de entorno
	envs = append(envs, "GIT_REPO="+template.Git.URL)
	envs = append(envs, "GIT_REPO2="+template.Git.URL2)
	if template.Git.Branch != "" {
		envs = append(envs, "GIT_BRANCH="+template.Git.Branch)
	}
	if template.Git.Username != "" {
		envs = append(envs, "GIT_USERNAME="+template.Git.Username)
	} else {
		envs = append(envs, "GIT_USERNAME="+config.Git.Username)
	}

	if template.Git.Password != "" {
		envs = append(envs, "GIT_PASSWORD="+template.Git.Password)
	} else {
		envs = append(envs, "GIT_PASSWORD="+config.Git.Password)
	}

	if template.Git.FullName != "" {
		envs = append(envs, "GIT_FULLNAME="+template.Git.FullName)
	} else {
		envs = append(envs, "GIT_FULLNAME="+config.Git.FullName)
	}

	if template.Git.Email != "" {
		envs = append(envs, "GIT_EMAIL="+template.Git.Email)
	} else {
		envs = append(envs, "GIT_EMAIL="+config.Git.Email)
	}

	if template.Gitlab.CreateMR {
		envs = append(envs, "GITLAB_CREATE_MR=true")
	}

	if template.Image == "" {
		template.Image = config.BaseImage
	}

	// determino la imagen a utiliar
	variant := ""
	if template.Variant != "alpine" {
		variant = template.Variant
	}
	lang := ""
	if variant != "" {
		variant += "-"
	}
	if template.Lang == "node" || template.Lang == "" {
		lang = "latest"
	} else {
		lang = template.Lang
	}

	template.Image += ":" + variant + lang

	envs = append(envs, helper.EnvMapToSlice(template.Env)...)

	cli, err := GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al crear contenedor")
		return err
	}

	// var initCommands [][]string
	mounts := make([]mount.Mount, 0)

	privileged := false
	if config.Dind.Enabled && template.Dind.Enabled {
		log.Warn().Msg("Iniciando Workspace en modo privilegiado!")
		privileged = true
	}
	if len(template.Tasks.Command) > 0 {
		if template.Ingress.Enabled {
			// Generate nginx certs
			if template.Ingress.Https {
				template.Tasks.Init = append(template.Tasks.Init, "mkdir -p /etc/nginx/certs && openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/nginx/certs/server.key -out  /etc/nginx/certs/server.crt -batch")
			}
			template.Tasks.Command = append(template.Tasks.Command, "/usr/sbin/nginx")
		}
		tasks := strings.Join(template.Tasks.Command, ";")
		envs = append(envs, "TASKS_COMMAND="+tasks)
	}
	if len(template.Tasks.Init) > 0 {
		tasks := strings.Join(template.Tasks.Init, ";")
		envs = append(envs, "TASKS_INI="+tasks)
	}

	//creando volumenes
	_, volumeTheia, err := cli.VolumeCreateLocal("kind__theia", "Almacena las preferencias del IDE Theia")
	if err != nil {
		log.Error().Err(err).Msg("Error al crear volumen theia")
		return err
	}
	mounts = append(mounts, mount.Mount{
		Type:     mount.TypeVolume,
		Source:   volumeTheia.Name,
		Target:   "/home/theia/.theia",
		ReadOnly: false,
	})

	_, volumeCerts, err := cli.VolumeCreateLocal("kind__certs", "Almacena los certificados SSL utilizado por los workspaces")
	if err != nil {
		log.Error().Err(err).Msg("Error al crear volumen certs")
		return err
	}
	mounts = append(mounts, mount.Mount{
		Type:     mount.TypeVolume,
		Source:   volumeCerts.Name,
		Target:   "/home/theia/certs",
		ReadOnly: false,
	})

	for _, v := range template.Volumes {
		partes := strings.Split(v, ":")

		src := strings.TrimSpace(partes[0])
		dst := strings.TrimSpace(partes[1])
		if src[0] == '/' || src[0] == '.' {
			log.Warn().Str("Source", src).Msg("Binds locales no soportados")
			continue
		}

		volumeName := "kind__" + src
		log.Debug().Interface("volumeName", volumeName).Msg("Creando Volumenes")
		_, _, err := cli.VolumeCreateLocal(volumeName, "")
		if err != nil {
			log.Error().Err(err).Str("volumen", volumeName).Msg("Error al crear volumen ")
			continue
		}
		mounts = append(mounts, mount.Mount{
			Type:     mount.TypeVolume,
			Source:   volumeName,
			Target:   dst,
			ReadOnly: false,
		})
	}

	log.Debug().Interface("mounts", mounts).Msg("Mounts")

	portMapping := make([]string, 0)
	puertos, _ := cli.ListUsedPorts()
	bindAddress := "0.0.0.0"

	basePort, err := strconv.Atoi(os.Getenv("KIND_BASE_PORT"))
	if err != nil {
		basePort = config.Network.BasePort
	}

	candidate := strconv.Itoa(basePort)
	log.Debug().Interface("basePort", basePort).Interface("ports", template.Ports).Send()

	var idePort string
	var ingressPort string
	var ingressEnabled string
	if template.Ports.IDE == "auto" {
		candidate = findFreePort(puertos, candidate)
	} else {
		candidate = template.Ports.Ingress
	}
	portMapping = append(portMapping, bindAddress+":"+candidate+":3000")
	idePort = candidate

	if template.Ports.Ingress != "" && template.Ports.Ingress != "none" && template.Ingress.Enabled {
		if template.Ports.Ingress == "auto" {
			candidate = findFreePort(puertos, candidate)
		} else {
			candidate = template.Ports.Ingress
		}
		portMapping = append(portMapping, bindAddress+":"+candidate+":3001")
		ingressPort = candidate
		if template.Ingress.Enabled {
			ingressEnabled = "true"
		} else {
			ingressEnabled = "false"
		}
	}

	// busco la imagen
	f := filters.NewArgs()
	if template.Image != "" {
		f.Add("reference", template.Image)
	}

	opts := types.ImageListOptions{
		All:     true,
		Filters: f,
	}

	imagelist, err := cli.ImageList(ctx, opts)
	if err != nil {
		log.Error().Err(err).Msg("Error al listar imagen")
		return err
	}

	if pull || len(imagelist) == 0 {
		reader, err := cli.ImagePull(template.Image)
		if err != nil {
			log.Error().Err(err).Msg("Descargar imagen")
			return err
		}
		defer reader.Close()
		io.Copy(os.Stdout, reader)
	}

	log.Trace().Interface("variables de entorno", envs).Msg("Creando Entorno")

	networkName := "kind__default"
	var networkId string
	networks, err := cli.NetworkList(ctx, types.NetworkListOptions{})
	if err != nil {
		return err
	}
	for _, n := range networks {
		if networkName == n.Name {
			networkId = n.ID
			break
		}
	}
	log.Debug().Interface("networkId", networkId).Msg("Creando Entorno")
	if networkId == "" {
		response, err := cli.NetworkCreate(ctx, networkName, types.NetworkCreate{
			EnableIPv6:     false,
			CheckDuplicate: true,
			Attachable:     true,
		})
		if err != nil {
			return err
		}
		networkId = response.ID
		log.Debug().Interface("networkId", networkId).Msg("Creando Red")
	}

	exposedPorts, portBindings, _ := natting.ParsePortSpecs(portMapping)
	containerConfig := &container.Config{
		Image:    template.Image,
		Hostname: name,
		Env:      envs,
		Labels: map[string]string{
			"KodeInDocker.isEnv":       "true",
			"KodeInDocker.name":        name,
			"KodeInDocker.dind":        "true",
			"KodeInDocker.idePort":     idePort,
			"KodeInDocker.ingressPort": ingressPort,
			"KodeInDocker.ingressTls":  ingressEnabled,
		},
		AttachStdout: true,
		AttachStderr: true,
		User:         "theia",
		ExposedPorts: exposedPorts,
	}

	extraHosts = append(extraHosts, template.ExtraHosts...)

	hostConfig := &container.HostConfig{
		RestartPolicy: container.RestartPolicy{
			Name: "no",
		},
		PortBindings: portBindings,
		Privileged:   privileged,
		Mounts:       mounts,
		ExtraHosts:   extraHosts,
		DNS:          config.Network.DNS,
		DNSOptions:   []string{"ndots:0"},
	}
	if privileged {
		hostConfig.UsernsMode = "host"
	}

	var endpointSettings = make(map[string]*network.EndpointSettings)
	endpointSettings[networkName] = &network.EndpointSettings{}
	networkConfig := &network.NetworkingConfig{
		EndpointsConfig: endpointSettings,
	}

	platform := &specs.Platform{
		Architecture: `amd64`,
		OS:           `linux`,
	}

	// creando contenedor
	result, err := cli.ContainerCreate(ctx, containerConfig, hostConfig, networkConfig, platform, "kind__"+name)
	if err != nil {
		log.Error().Err(err).Msg("Error al crear contenedor")
		return err
	}
	for _, w := range result.Warnings {
		log.Warn().Msg(w)
	}
	containerID := result.ID

	// Generate nginx configuration if provided in template
	if template.Ingress.Http != "" {
		envs := helper.MergeMaps(config.Env, template.Env)
		template.Ingress.Http = helper.ReplaceVars(template.Ingress.Http, envs)
		var buf bytes.Buffer
		tw := tar.NewWriter(&buf)
		err = tw.WriteHeader(&tar.Header{
			Name: "default.conf",
			Mode: 0777,
			Size: int64(len(template.Ingress.Http)),
		})
		if err != nil {
			return err
		}
		tw.Write([]byte(template.Ingress.Http))
		tw.Close()
		// enviando archivo al contenedor
		err = cli.Client.CopyToContainer(ctx, containerID, "/etc/nginx/http.d", &buf, types.CopyToContainerOptions{
			AllowOverwriteDirWithFile: true,
		})
		if err != nil {
			return err
		}
	}

	/*
		nginxConfig, err := helper.GenerateNginxConf(template.Ingress.Http)
		if err != nil {
			return err
		}
	*/

	log.Info().Msg("Iniciando Contenedor")
	if err := cli.ContainerStart(ctx, containerID, types.ContainerStartOptions{}); err != nil {
		log.Error().Err(err).Msg("Error al iniciar contenedor")
		return err
	}

	if config.AllowSudo && template.Sudo {
		log.Warn().Msg("Habilitando Sudo")
		if err := EnableSudo(containerID); err != nil {
			log.Error().Err(err).Msg("Error al habilitar sudo")
		}
	} else {
		log.Info().Msg("Deshabilitando sudo")
		if err := DisableSudo(containerID); err != nil {
			log.Error().Err(err).Msg("Error al deshabilitar sudo")
		}
	}

	if config.Dind.Enabled && template.Dind.Enabled {
		privileged = true
		if err := StartDockerd(containerID); err != nil {
			log.Error().Err(err).Msg("Error al iniciar docker en contenedor")
		}
	}

	if wait {
		WaitWorkspace(containerID)
	}

	log.Info().Msg("Listo!")

	return nil
}

// DisableSudo Deshabilita sudo en el contenedor
func DisableSudo(id string) error {
	cli, err := GetInstance()
	if err != nil {
		return err
	}
	ctx := context.Background()
	log.Info().Msg("Deshabilitando Sudo")
	if err := cli.ContainerExec(ctx, id, []string{"apk", "del", "sudo"}); err != nil {
		return err
	}
	return nil
}

// EnableSudo Habilita sudo en el contenedor
func EnableSudo(id string) error {
	cli, err := GetInstance()
	if err != nil {
		return err
	}
	ctx := context.Background()
	log.Info().Msg("Habiltando Sudo")
	if err := cli.ContainerExec(ctx, id, []string{"apk", "add", "--no-cache", "sudo"}); err != nil {
		return err
	}
	return nil
}

// EnableSudo Inicia dockerd dentro del contenedor (dind)
func StartDockerd(id string) error {
	cli, err := GetInstance()
	if err != nil {
		return err
	}
	ctx := context.Background()
	if err := cli.ContainerExec(ctx, id, []string{"/usr/bin/dockerd"}); err != nil {
		return err
	}
	return nil
}

func WaitWorkspace(id string) error {
	cli, err := GetInstance()
	if err != nil {
		return err
	}
	ctx := context.Background()
	containerStatus, _ := cli.ContainerInspect(ctx, id)
	log.Info().Msg("Esperando que el entorno esté listo")
	log.Debug().Str("status", containerStatus.State.Health.Status).Msg("")
	for containerStatus.State.Health.Status != "healthy" {
		time.Sleep(5 * time.Second)
		containerStatus, _ = cli.ContainerInspect(ctx, id)
		log.Debug().Str("status", containerStatus.State.Health.Status).Msg("")
	}
	return nil
}

func findFreePort(list []int, candidate string) string {
	cand, err := strconv.Atoi(candidate)
	if err != nil {
		cand = 3010
	}
	// i := 0

	for {
		cand++
		ocupado := false
		for _, p := range list {
			if p == cand {
				ocupado = true
				break
			}
		}
		if !ocupado {
			return strconv.Itoa(cand)
		}
		// log.Debug().Interface("cand", cand).Interface("pos", pos).Interface("len", len(list)).Msg("buscando puerto libre")
	}

}
