package docker

import (
	"context"
	"github.com/docker/docker/api/types"
	// "github.com/docker/docker/api/types/filters"
	"io"
)

//ImagePull pulls image from registry
func (c *Connection) ImagePull(image string) (io.ReadCloser, error) {

	return c.Client.ImagePull(context.Background(), image, types.ImagePullOptions{})

}
