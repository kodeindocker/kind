package docker

import (
	"context"
	"errors"
	// "github.com/docker/docker/api/types"
)

//ContainerRestartByName Reinicia un Contenedor
func (c *Connection) ContainerRestartByName(name string) error {
	containers, err := c.Ps(name)
	if err != nil {
		return err
	}
	if len(containers) == 0 {
		return errors.New("Entorno '" + name + "' no encontrado")
	}
	ctx := context.Background()
	container := containers[0]
	id := container.ID

	err = c.Client.ContainerRestart(ctx, id, nil)
	if err != nil {
		return err
	}

	dind := container.Labels["KodeInDocker.dind"]
	if dind == "true" {
		if err := StartDockerd(id); err != nil {
			return err
		}
	}

	return nil
}
