package docker

import (
	"context"
	// "errors"
	"github.com/docker/docker/api/types/container"
	"github.com/rs/zerolog/log"
)

//ContainerWait Espera que un contenedor esté activo
func (c *Connection) ContainerWaitStarted(id string) error {
	ctx := context.Background()
	statusCh, errCh := c.ContainerWait(ctx, id, container.WaitConditionNotRunning)
	select {
	case err := <-errCh:
		if err != nil {
			log.Fatal().Err(err).Msg("Error en contenedor")
		}
	case <-statusCh:
		log.Debug().Msg("ok!")
	}
	return nil
}
