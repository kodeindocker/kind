package docker

import (
	"context"
	"sort"
	"strconv"

	"github.com/docker/docker/api/types"
	"github.com/docker/docker/api/types/filters"
)

// Ps devuelve la lista de contenedores
// solo devuelve los marcados como entornos KodeInDocker
// @param name nombre del entorno, si se omite se muestran todos los entornos
func (c *Connection) Ps(name string) ([]types.Container, error) {
	ctx := context.Background()
	f := filters.NewArgs()

	f.Add("label", "KodeInDocker.isEnv=true")
	if name != "" {
		f.Add("label", "KodeInDocker.name="+name)

	}

	opts := types.ContainerListOptions{
		All:     true,
		Filters: f,
	}

	containers, err := c.ContainerList(ctx, opts)
	if err != nil {
		return containers, err
	}
	return containers, nil
}

// ListUsedPorts devuelve la lista de puertos usados por todos los contenedores (aunque no sean de kind)
func (c *Connection) ListUsedPorts() ([]int, error) {
	res := []int{}
	ctx := context.Background()
	f := filters.NewArgs()

	opts := types.ContainerListOptions{
		All:     true,
		Filters: f,
	}

	containers, err := c.Client.ContainerList(ctx, opts)
	if err != nil {
		return res, err
	}

	for _, container := range containers {
		cData, err := c.ContainerInspect(ctx, container.ID)
		if err != nil {
			continue
		}
		portsMap := cData.HostConfig.PortBindings
		for _, pm := range portsMap {
			for _, p := range pm {
				pInt, err := strconv.Atoi(p.HostPort)
				if err != nil {
					continue
				}
				res = append(res, pInt)
			}
		}
	}
	sort.Ints(res)

	return res, err
}
