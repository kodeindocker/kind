# [WIP] KodeInDocker
[WIP] Develop in docker using your browser

## Outline

- [Overview](#overview)
    - [Supported Languages](#supported-languages)
    - [Termplates](#templates)
- [Installation](#installation)
- [Command Line](#command-line)
- [Web UI](#web-ui)

<br />

## Overview
KIND allows management of development environments
Each development environment consists of a docker container with [Eclipse Theia IDE](https://theia-ide.org/) and your preferred programming language

### Supported Languages
| Language | Image |
|--------|-----------|
| Node | registry.gitlab.com/kodeindocker/theia |
| Node+Golang | registry.gitlab.com/kodeindocker/theia:go |


### Templates
KiND uses templates to generate development environments.
See [Templates](doc/Templates.md)


## Installation
See 
* [Bare Metal Linux Install](doc/Baremetal-Linux.md)
* [Docker Install](doc/Docker.md)


## Web UI
Comming Soon


## Command Line
1. create a template file, see [Templates](doc/Templates.md)

### Create a Development Environment
`./kind up -t path/to/yaml <name>`

Example

`./kind up -t templates/example.yml example1`


### List running Environments
`./kind ps`


```sh
Name            Container Name       Status          Ports
-----------------------------------------------------------------------
example         kind_example         running         0.0.0.0:6001->3000 
```

### Delete an Environment
`./kind down [--force] <name>`

Also, you can delete an environment by using `docker rm [--force] <container_name>`

### Stop an Environment
`./kind stop <name>`

Also, you can stop an environment by using `docker stop <container_name>`

### Start a previously stopped environment
`./kind start <name>`

Also, you can start an environment by using `docker start <container_name>`

### Show logs
`./kind logs [-f] [--tail=xx] <name>`

Also, you can show logs by using `docker logs [-f] [--tail=xx] <container_name>`




