# Compila la porción web del proyecto
build-web: web/src
	cd web; \
	yarn install; \
	yarn build;

# compila la version golang
build: build-web
	go mod vendor
	go build

# instala el binario en /home/project/bin
install: build
	go install

# inicia kind en modo HMR para desarrollo
dev: 
	go build && go install && kind config &> /dev/null && \
	reflex -s -r '\.go$$' -- sh -c "go build && go install && kind serve"

dev-web:
	(cd web && yarn serve)