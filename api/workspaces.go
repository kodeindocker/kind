package api

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/docker/docker/api/types"
	"github.com/go-chi/chi"
	"github.com/rs/zerolog/log"

	// helper "gitlab.com/kodeindocker/kind/helper"
	"gitlab.com/kodeindocker/kind/docker"
	"gitlab.com/kodeindocker/kind/loader"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

func Workspaces(templateFolder string) http.Handler {
	TemplateFolder = templateFolder
	r := chi.NewRouter()
	r.Get("/", listWorkspaces)
	r.Post("/", createWorkspace)
	r.Get("/{name}", getWorkspace)
	r.Delete("/{name}", deleteWorkspace)
	r.Get("/{name}/wait", waitWorkspace)
	r.Put("/{name}/stop", stopEnvironment)
	r.Put("/{name}/start", startWorkspace)
	r.Put("/{name}/restart", restartWorkspace)
	return r
}

func listWorkspaces(w http.ResponseWriter, r *http.Request) {
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener lista de instancias")
		render.Render(w, r, render.NewGenericErrorResponse("Error al obtener lista de instancias"))
		return
	}
	containers, err := cli.Ps("")
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener lista de instancias")
		render.Render(w, r, render.NewGenericErrorResponse("Error al obtener lista de instancias"))
		return
	}

	var response []Workspace = make([]Workspace, 0)
	for _, c := range containers {
		response = append(response, container2Workspace(c))
	}

	render.Render(w, r, render.NewSuccessResponse(response))
}

func getWorkspace(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener lista de instancias")
		render.Render(w, r, render.NewGenericErrorResponse("Error al obtener lista de instancias"))
		return
	}
	containers, err := cli.Ps(name)
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener entorno")
		render.Render(w, r, render.NewGenericErrorResponse("Entorno no encontrado"))
		return
	}
	if len(containers) == 0 {
		render.Render(w, r, render.NewErrorNotFoundResponse("Entorno no encontrado"))
		return
	}

	var response Workspace = container2Workspace(containers[0])

	render.Render(w, r, render.NewSuccessResponse(response))
}

func stopEnvironment(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	log.Info().Interface("name", name).Msg("Deteniendo entorno")
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al conectar a docker")
		render.Render(w, r, render.NewErrorNotFoundResponse("Error interno docker"))

		return
	}

	err = cli.ContainerStopByName(name)
	if err != nil {
		log.Error().Err(err).Msg("Error al detener entorno")
		render.Render(w, r, render.NewErrorNotFoundResponse("Error al detener entorno"))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}

func startWorkspace(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	log.Info().Interface("name", name).Msg("Iniciando entorno")
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al conectar a docker")
		render.Render(w, r, render.NewErrorNotFoundResponse("Error interno docker"))

		return
	}

	ctx := context.Background()
	err = cli.ContainerStartByName(ctx, name)
	if err != nil {
		log.Error().Err(err).Msg("Error al iniciar entorno")
		render.Render(w, r, render.NewErrorNotFoundResponse("Error al iniciar entorno"))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}

func restartWorkspace(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	log.Info().Interface("name", name).Msg("Reiniciando entorno")
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al conectar a docker")
		render.Render(w, r, render.NewErrorNotFoundResponse("Error interno docker"))

		return
	}

	err = cli.ContainerRestartByName(name)
	if err != nil {
		log.Error().Err(err).Msg("Error al reiniciar entorno")
		render.Render(w, r, render.NewErrorNotFoundResponse("Error al reiniciar entorno"))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}

func createWorkspace(w http.ResponseWriter, r *http.Request) {
	rp := middlewares.GetContextParams(r)
	payload := rp.RequestBody.Data
	log.Info().Interface("payload", payload).Msg("Creando Entorno")

	bytes, err := json.Marshal(payload)
	if err != nil {
		log.Error().Err(err).Msg("Error al leer body de request")
		render.Render(w, r, render.NewErrorBadRequestResponse("Request Invalido"))
		return
	}

	data := requestEntorno{}
	err = json.Unmarshal(bytes, &data)
	if err != nil {
		log.Error().Err(err).Msg("Error al leer body de request")
		render.Render(w, r, render.NewErrorBadRequestResponse("Request Invalido"))
		return
	}

	err = docker.CreateWorkspace(data.Name, &data.Template, data.Pull, false)
	if err != nil {
		log.Error().Err(err).Msg("Error al crear entorno")
		render.Render(w, r, render.NewGenericErrorResponse("Error al crear entorno"))
		return
	}

	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener lista de instancias")
		render.Render(w, r, render.NewGenericErrorResponse("Error al obtener lista de instancias"))
		return
	}

	containers, err := cli.Ps(data.Name)
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener entorno")
		render.Render(w, r, render.NewGenericErrorResponse("Entorno no encontrado"))
		return
	}
	if len(containers) == 0 {
		render.Render(w, r, render.NewErrorNotFoundResponse("Entorno no encontrado"))
		return
	}

	var response Workspace = container2Workspace(containers[0])

	render.Render(w, r, render.NewSuccessResponse(response))
}

func deleteWorkspace(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	log.Info().Interface("name", name).Msg("Eliminando entorno")
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al conectar a docker")
		render.Render(w, r, render.NewErrorNotFoundResponse("Error interno docker"))

		return
	}

	err = cli.ContainerDown(name, true)
	if err != nil {
		log.Error().Err(err).Msg("Error al eliminar entorno")
		render.Render(w, r, render.NewErrorNotFoundResponse("Error al eliminar entorno"))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(""))
}

func waitWorkspace(w http.ResponseWriter, r *http.Request) {
	name := chi.URLParam(r, "name")
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al conectar a docker")
		render.Render(w, r, render.NewErrorNotFoundResponse("Error interno docker"))
		return
	}

	containers, err := cli.Ps(name)
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener entorno")
		render.Render(w, r, render.NewGenericErrorResponse("Entorno no encontrado"))
		return
	}
	if len(containers) == 0 {
		render.Render(w, r, render.NewErrorNotFoundResponse("Entorno no encontrado"))
		return
	}

	var response Workspace = container2Workspace(containers[0])
	docker.WaitWorkspace(response.ContainerID)

	containers, err = cli.Ps(name)
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener entorno")
		render.Render(w, r, render.NewGenericErrorResponse("Entorno no encontrado"))
		return
	}
	if len(containers) == 0 {
		render.Render(w, r, render.NewErrorNotFoundResponse("Entorno no encontrado"))
		return
	}

	response = container2Workspace(containers[0])

	render.Render(w, r, render.NewSuccessResponse(response))
}

func container2Workspace(c types.Container) Workspace {
	e := Workspace{
		Name:        c.Names[0][7:],
		ContainerID: c.ID,
		State:       c.State,
		Status:      c.Status,
	}
	for _, p := range c.Ports {
		e.Ports = append(e.Ports, PortMap{
			Public:  p.PublicPort,
			Private: p.PrivatePort,
		})
	}

	return e
}

type Workspace struct {
	Name        string    `json:"name"`
	ContainerID string    `json:"containerID"`
	Ports       []PortMap `json:"ports"`
	State       string    `json:"state"`
	Status      string    `json:"status"`
}

type PortMap struct {
	Public  uint16 `json:"public"`
	Private uint16 `json:"private"`
}

type requestEntorno struct {
	Name     string `json:"name"`
	Pull     bool
	Template loader.TemplateConfig `json:"template"`
}
