package api

import (
	"context"
	"encoding/json"
	"net/http"

	// "strings"
	// "path/filepath"
	// "os"

	"github.com/go-chi/chi"
	"github.com/rs/zerolog/log"

	"github.com/docker/docker/api/types/filters"
	"gitlab.com/kodeindocker/kind/docker"

	// "gitlab.com/kodeindocker/kind/loader"
	// "gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/render"
)

func Volumes() http.Handler {
	r := chi.NewRouter()
	r.Get("/", volumeList)
	// r.Get("/{name}", volumeGet)
	r.Post("/", volumeCreate)
	r.Delete("/{name}", volumeDelete)
	return r
}

func volumeList(w http.ResponseWriter, r *http.Request) {
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al conectar con docker")
		render.Render(w, r, render.NewGenericErrorResponse("Error al conectar con docker"))
		return
	}

	ctx := context.Background()
	f := filters.NewArgs()
	response, err := cli.VolumeList(ctx, f)
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener lista de volumenes")
		render.Render(w, r, render.NewGenericErrorResponse("Error al obtener lista de volumenes"))
		return
	}
	var index = make(map[string]*volumeInfo)
	var list []*volumeInfo

	for _, v := range response.Volumes {
		vi := volumeInfo{
			Name:      v.Name,
			CreatedAt: v.CreatedAt,
			Labels:    v.Labels,
		}
		index[v.Name] = &vi
		list = append(list, &vi)
	}

	containers, err := cli.Ps("")
	if err != nil {
		log.Error().Err(err).Msg("Error al obtener lista de contenedores")
		render.Render(w, r, render.NewGenericErrorResponse("Error al obtener lista de contenedores"))
		return
	}
	for _, c := range containers {
		for _, m := range c.Mounts {
			if v, ok := index[m.Name]; ok {
				v.UsedBy = append(v.UsedBy, c.Names...)
			}
		}
	}

	if err != nil {
		log.Error().Err(err).Msg("Error al obtener lista de volumenes")
		render.Render(w, r, render.NewGenericErrorResponse("Error al obtener lista de volumenes"))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(list))
}

func volumeCreate(w http.ResponseWriter, r *http.Request) {
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al conectar con docker")
		render.Render(w, r, render.NewGenericErrorResponse("Error al conectar con docker"))
		return
	}
	rp := middlewares.GetContextParams(r)
	js, err := json.Marshal(rp.RequestBody.Data)
	if err != nil {
		log.Error().Err(err).Msg("Error al crear volumen")
		render.Render(w, r, render.NewGenericErrorResponse("Error al crear volumen"))
		return
	}
	var payload []volume
	err = json.Unmarshal(js, &payload)
	if err != nil {
		log.Error().Err(err).Msg("Error al crear volumen")
		render.Render(w, r, render.NewGenericErrorResponse("Error al crear volumen"))
		return
	}

	if len(payload) > 0 {
		_, _, err := cli.VolumeCreateLocal("kind__"+payload[0].Name, payload[0].Description)
		if err != nil {
			log.Error().Err(err).Msg("Error al crear volumen")
			render.Render(w, r, render.NewGenericErrorResponse("Error al crear volumen"))
			return
		}
	}

	render.Render(w, r, render.NewSuccessResponse(payload))

}

type volume struct {
	Name        string `json:"name"`
	Description string `json:"description"`
}

func volumeDelete(w http.ResponseWriter, r *http.Request) {
	cli, err := docker.GetInstance()
	if err != nil {
		log.Error().Err(err).Msg("Error al conectar con docker")
		render.Render(w, r, render.NewGenericErrorResponse("Error al conectar con docker"))
		return
	}
	name := chi.URLParam(r, "name")
	log.Warn().Interface("name", name).Msg("Eliminando volumen")

	ctx := context.Background()
	_, err = cli.VolumeRemoveIfExists(ctx, "kind__"+name)
	if err != nil {
		log.Error().Err(err).Msg("Error al eliminar volumen")
		render.Render(w, r, render.NewGenericErrorResponse("Error al eliminar volumen"))
		return
	}
}

type volumeInfo struct {
	Name      string            `json:"name"`
	CreatedAt string            `json:"createdAt"`
	UsedBy    []string          `json:"usedBy"`
	Labels    map[string]string `json:"labels"`
}
