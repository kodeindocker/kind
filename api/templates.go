package api

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/rs/zerolog/log"

	"path/filepath"
	"strings"

	"gitlab.com/kodeindocker/kind/loader"
	"gitlab.com/mind-framework/core/mind-core-api/middlewares"
	"gitlab.com/mind-framework/core/mind-core-api/render"

	// "log"
	"os"
)

const (
	ErrorInvalidPayload = "Plantilla Inválida"
	TemplateErrorSaving = "Error al Salvar Archivos"
)

var TemplateFolder = "templates"

func Templates(templateFolder string) http.Handler {
	TemplateFolder = templateFolder
	r := chi.NewRouter()
	r.Get("/", listTemplates)
	r.Get("/{plantilla}", getTemplate)
	r.Post("/{plantilla}", createTemplate)
	r.Delete("/{plantilla}", deleteTemplate)
	return r
}

// devuelve la lista de plantillas configuradas en la carpeta templates
func listTemplates(w http.ResponseWriter, r *http.Request) {
	matches, err := filepath.Glob(TemplateFolder + "/*.yml")
	if err != nil {
		render.Render(w, r, render.NewGenericErrorResponse("Error al obtener lista de templates"))
		return
	}

	templates := make([]map[string]string, 0)
	for _, m := range matches {
		t := strings.TrimPrefix(m, TemplateFolder+"/")
		t = strings.TrimSuffix(t, ".yml")
		templates = append(templates, map[string]string{
			"name": t,
		})
	}

	render.Render(w, r, render.NewSuccessResponse(templates))

}

func getTemplate(w http.ResponseWriter, r *http.Request) {
	// TODO: sanitizar
	path := TemplateFolder + `/` + chi.URLParam(r, "plantilla") + ".yml"

	template, err := loader.LoadTemplate(path)
	if err != nil {
		log.Error().Err(err).Msg("Error al cargar template")
		render.Render(w, r, render.NewGenericErrorResponse("Error al cargar Template"))
		return
	}

	render.Render(w, r, render.NewSuccessResponse(template))
}

func createTemplate(w http.ResponseWriter, r *http.Request) {
	plantilla := chi.URLParam(r, "plantilla")

	rp := middlewares.GetContextParams(r)
	payload := rp.RequestBody.Data

	byteData, err := json.Marshal(payload)
	if err != nil {
		log.Error().Err(err).Msg(ErrorInvalidPayload)
		render.Render(w, r, render.NewGenericErrorResponse(ErrorInvalidPayload))
		return
	}

	if err := loader.SaveTemplate(TemplateFolder+"/"+plantilla+".yml", byteData); err != nil {
		render.Render(w, r, render.NewGenericErrorResponse(TemplateErrorSaving))
	}

	render.Render(w, r, render.NewSuccessResponse("ok"))
}

func deleteTemplate(w http.ResponseWriter, r *http.Request) {
	// TODO: sanitizar
	plantilla := TemplateFolder + `/` + chi.URLParam(r, "plantilla") + ".yml"
	if err := os.Remove(plantilla); err != nil {
		log.Error().Err(err).Msg("Error al eliminar template")
		render.Render(w, r, render.NewGenericErrorResponse("Error al eliminar Template"))
		return
	}
	render.Render(w, r, render.NewSuccessResponse("ok"))
}
