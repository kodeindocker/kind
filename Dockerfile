FROM alpine
RUN apk add --no-cache docker && \
    adduser -h /home/kind -u 1000 -s /bin/sh -D kind && \
    addgroup kind docker
WORKDIR "/app"
USER kind
COPY dist/kind /app/kind
COPY entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
