package loader

import (
	"github.com/rs/zerolog/log"
	"io/ioutil"

	// "gitlab.com/kodeindocker/kind/helper"
	"gopkg.in/yaml.v3"
)

//AppConfig holds the application  configuration
type AppConfig struct {
	Version   string
	BaseImage string `yaml:"base_image"`
	Variants  []string
	Langs     []string
	Dind      struct {
		Enabled         bool
		AllowPrivileged bool `yaml:"allow_privileged"`
	}
	AllowSudo bool `yaml:"allow_sudo"`
	Git       GitConfig
	LogLevel  int
	Network   struct {
		BasePort int      `yaml:"base_port"`
		DNS      []string `yaml:"dns"`
	} `yaml:"network"`
	Env map[string]string
}

//LoadConfig loads the configuratioin file
func LoadConfig(filename string) (*AppConfig, error) {
	content, err := ioutil.ReadFile(filename) // the file is inside the local directory
	if err != nil {
		return nil, err
	}

	var c AppConfig
	err = yaml.Unmarshal(content, &c)
	if err != nil {
		return nil, err
	}

	log.Debug().Interface("config", c).Send()

	if c.AllowSudo {
		log.Warn().Msg("Se permitirá ejecutar comandos como sudo en los entornos")
	}

	if c.Dind.Enabled {
		if c.Dind.AllowPrivileged {
			log.Warn().Msg("DIND: Se permitirá ejecutar contenedores con privilegios elevados")
		}
	}

	if c.BaseImage == "" {
		c.BaseImage = "registry.gitlab.com/kodeindocker/theia"
	}

	if c.Network.BasePort == 0 {
		c.Network.BasePort = 3001
	}

	if len(c.Variants) == 0 {
		c.Variants = append(c.Variants, "alpine")
	}
	return &c, nil
}

