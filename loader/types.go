package loader

// IngressServer represents an ingress server section
type IngressServer struct {
	Server    string            `yaml:"server" json:"server"`
	Locations []IngressLocation `yaml:"locations" json:"locations"`
}

// IngressLocation represents an ingress location section
type IngressLocation struct {
	Location string `yaml:"location" json:"location"`
	Backend  string `yaml:"backend" json:"backend"`
	Config   string `yaml:"config" json:"config"`
}

