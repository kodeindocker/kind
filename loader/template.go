package loader

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"github.com/rs/zerolog/log"
	"gitlab.com/kodeindocker/kind/helper"
	"gopkg.in/yaml.v3"
)

// TemplateConfig holds the configuration of a template
type TemplateConfig struct {
	Image       string `yaml:"image" json:"image"`
	Description string `yaml:"description" json:"description"`
	Variant     string `yaml:"variant" json:"variant"`
	Lang        string `yaml:"lang" json:"lang"`
	Sudo        bool   `yaml:"sudo" json:"sudo"`
	Dind        struct {
		Enabled    bool `yaml:"enabled" json:"enabled"`
		Privileged bool `yaml:"privileged" json:"privileged"`
	} `yaml:"dind" json:"dind"`
	Tasks struct {
		Init    []string `yaml:"init" json:"init"`
		Command []string `yaml:"command" json:"command"`
	} `yaml:"tasks" json:"tasks"`
	Gitlab struct {
		CreateMR bool `yaml:"create_mr" json:"createMR"`
	} `yaml:"gitlab" json:"gitlab"`
	Git struct {
		URL       string `yaml:"url" json:"url"`
		URL2      string `yaml:"url2" json:"url2"`
		GitConfig `yaml:",inline"`
		Branch    string `yaml:"branch" json:"branch"`
	} `yaml:"git" json:"git"`
	Ports struct {
		IDE     string `yaml:"ide" json:"ide"`
		Ingress string `yaml:"ingress" json:"ingress"`
		// Extra   []PortMapping `yaml:"extra" json:"extra"`
	} `yaml:"ports" json:"ports"`
	Ingress struct {
		Enabled bool   `yaml:"enabled" json:"enabled"`
		Https   bool   `yaml:"https" json:"https"`
		Http    string `yaml:"http" json:"http"`
	} `yaml:"ingress" json:"ingress"`
	Env        map[string]string `yaml:"env" json:"env"`
	Volumes    []string          `yaml:"volumes" json:"volumes"`
	ExtraHosts []string          `yaml:"extra_hosts" json:"extraHosts"`
}

func (t *TemplateConfig) ParseVars(vars map[string]string) error {
	payload, err := yaml.Marshal(t)
	if err != nil {
		return err
	}
	str := string(payload)
	str = helper.ReplaceVars(str, vars)
	if err := yaml.Unmarshal([]byte(str), t); err != nil {
		return err
	}

	return nil
}

type GitConfig struct {
	Username string `yaml:"username" json:"username"`
	Password string `yaml:"password" json:"password"`
	FullName string `yaml:"fullname" json:"fullname"`
	Email    string `yaml:"email" json:"email"`
}

// PortMapping holds information of a individual port mapping
type PortMapping struct {
	Public  string `yaml:"public" json:"public"`
	Private int    `yaml:"private" json:"private"`
}

// LoadTemplate loads a template from file into a TemplateConfig object
func LoadTemplate(filename string) (*TemplateConfig, error) {
	content, err := ioutil.ReadFile(filename) // the file is inside the local directory
	if err != nil {
		return nil, err
	}

	// var envs []string

	var t TemplateConfig
	err = yaml.Unmarshal(content, &t)
	if err != nil {
		return nil, err
	}

	// if t.Ports.Ingress == "" {
	// 	t.Ports.Ingress = "auto"
	// }

	log.Debug().Interface("template", t).Msg("Cargando Plantilla")

	return &t, nil
}

// SaveTemplate save template in yaml
func SaveTemplate(name string, content []byte) error {
	log.Debug().Str("name", name).Bytes("content", content).Msg("guardando template")

	t := TemplateConfig{}

	err := json.Unmarshal(content, &t)
	if err != nil {
		return err
	}

	f, err := os.Create(name)
	if err != nil {
		return err
	}
	defer f.Close()

	payload, err := yaml.Marshal(t)
	if err != nil {
		log.Error().Err(err).Msg("Error al guardar template")
		return err
	}
	f.Write(payload)

	return nil
}
